#encoding:utf-8
require "json"
require "mechanize"
require 'net/smtp'
require 'time'

Dir.chdir(File.dirname(__FILE__))
f = File.open("config.json","r")
	config = JSON.parse(f.read())
f.close()

def send_mail(subject, body, to, from, pass)
	begin
		if not to.empty?
			msg = "Subject: #{subject}\n\n#{body}"
			smtp = Net::SMTP.new 'smtp.gmail.com', 587
			smtp.enable_starttls
			smtp.start('smtp.gmail.com', from, pass, :login) do
				smtp.send_message(msg, from, to)
			end
		end
	rescue
		File.open("err.log", "a") do |f|
			f.puts "#{Time.now.getlocal("+08:00")}\tsend to #{to} failed.\t\tsubject: #{subject}\tbody: #{body}\n"
		end
	end
end

def do_mission(account, password, alert_email)
	http = Mechanize.new
	http.user_agent_alias = 'Mac Safari'
	# http.set_proxy('localhost', 8888)
	successed = false
	page = http.get( "http://www.v2ex.com/signin" )
	if forms = page.forms then
		if forms.length > 1 then
			login_form = forms[1]
			if login_form.method == "POST" and login_form.action == "/signin" then
				login_form['u'] = account
				login_form['p'] = password
				login_response_page = login_form.submit()

				screen_name = login_response_page.at("//span[@class='bigger']/a/text()").to_s
				if screen_name == account then
					mission_page = http.get("http://www.v2ex.com/mission/daily")
					# puts mission_page.body
					location = mission_page.at("//input[@class='super normal button']/@onclick").to_s
					match = /'([^']*)/.match(location)
					if match
						finish_mission_page = http.get(match[1])
						message = finish_mission_page.at("//div[@class='message']/text()").to_s
						if message.include?("已成功领取每日登录奖励")
							successed = true
						end
					end
				end
			end
		end
	end

	if not successed
		puts "#{account} mission failed"
		raise "mission failed"
	else
		puts "#{account} mission accomplished"
	end
end

config["accounts"].each do |account_info|
	begin
		account = account_info["account"]
		password = account_info["password"]
		alert_email = account_info["alert_email"]
		do_mission(
			account,
			password,
			alert_email
		)
	rescue
		send_mail(
			"#{account}未登录成功",
			"v2ex每日奖励领取失败#{Time.now.getlocal("+08:00")}",
			alert_email,
			config["gmail"],
			config["gmailpass"]
		)
	end
end
